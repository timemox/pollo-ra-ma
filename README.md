![homer_pulsador.jpg](https://bitbucket.org/repo/pLgy66/images/2409044652-homer_pulsador.jpg)

# README #

* * *

## REQUISITOS ##

* Drush
* Terminus

* * *

## INSTALACIÓN ##

1. Copia estos scripts donde quieras.
2. En */bin* añade a cada script un enlace simbólico. (Ej: ln -s */ruta-scripts/createtheme* createtheme)

* * *

## RESUMEN ##

**renameior:** Utilidad para refactorizar strings.

**configsite:** Nos deja en local, el estado actual del microsite que hay en dev de Pantheon.

**createtheme:** Crea un subtema bootstrap-sass.

**createsite:** Crea un microsite D8 en Pantheon, lo clona en local, le añade un subtema bootstrap-sass y lo sube a Pantheon.

* * *

## renameitor ##

Reemplaza un string por otro. Tanto si el string aparece en el nombre del fichero, como dentro del archivo. Es recursivo, por lo que afecta también a todos los subdirectorios desde donde ejecutemos renameitor.

**Uso:** *renameitor pepe juan*

* * *

## configsite ##

Reconstruye/actualiza en local un microsite de Pantheon. (Debe ejecutarse dentro de una carpeta del microsite)

**Uso:** *configsite*

**Pasos que realiza:**

1. Configura el Virtual Host si detecta que no existe en */etc/apache2/sites-available*.
2. Actualiza si hace falta el fichero *etc/hosts*
3. Reinicia el servicio apache2.
4. Si no existe la carpeta *sites/default/files* la crea y le da permisos, añade dentro estas carpetas: private y translations.
5. Si no existe la carpeta *sites/default/config* la crea y le da permisos.
6. Si no existe el fichero *sites/default/settings.local.php* lo crea y configura.
7. Activa el modo Debug. Si el fichero *sites/default/services.yml* no existe, lo crea y configura.
8. Al fichero *sites/default/settings.php* le añade una linea para que funcione correctamente el microsite.
9. Integracion continua: Si no existe el fichero *post-merge.sh*, configura la integración continua.
10. Actualiza el fichero con los drush alias de Pantheon.
11. Backup del microsite en Pantheon.
12. Descarga la BBDD de Pantheon y sobreescribe la BBDD de local.
13. Descarga la carpeta *sites/default/files* de Pantheon y la sobreescribe en local.
14. Limpia caches.
15. *Drush cex* en local (Al copiar la BBDD es posible que nuestros fichero en local no esten actualizados).

* * *

## createtheme ##

Crea un subtema bootstrap-sass. 

**Uso:** *createtheme pepe*

**Pasos que realiza:**

1. Descarga e instala si es necesario el tema bootstrap.
2. Crea un subtema desde themes/bootstrap/starterkits/sass
3. Refactoriza los nombre de ficheros y textos con el nombre del tema nuevo.
4. Descarga e instala la libreria bootstrap
5. Configura el SASS/COMPASS
6. Configura el CSS/JS del fichero THEMENAME.libraries.yml 
7. Crea el fichero js/scripts.js con la estructura básica.
8. Pregunta si queremos hacer predeterminado este tema.
9. Limpia caches.

* * *

## createsite ##

**UNDERCONTRUCTION** Crea un microsite Drupal 8 en Pantheon, despues lo clona y configura en local, le añade un subtema bootstrap-sass y lo sincroniza de nuevo en Pantheon.

**Uso:** *createsite pepe*

**Pasos que realiza:**

1. Crea el microsite en Pantheon
2. Ejecuta la instalación standard de Drupal 8.
3. Cambia de SFTP a GIT.
4. Clona el microsite a local.
5. Lanza el script *configsite* para configurar el microsite en local.
6. Lanza el script *createtheme* para crear un subtema bootstrap-sass en local.
7. Crea el primer push para sincronizar el microsite local a Pantheon.